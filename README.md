# CAN Transceiver
Auteur : Baptiste Maheut<br>
Version : 1

![Modele 3D](model.png "Modele 3D")

## Introduction
L'objectif de ce transmetteur CAN est de fournir une liaison physique au CAN pour les Nucleo L432KC et plus généralement pour les cartes électroniques au format Arduino Nano. La terminaison 120 Ohm peut être activée en connectant les jumpers de part et d'autre de la carte.

Dans l'état actuel, ce PCB est **fonctionnel**.

Les branches `prototype` et `prototype-daisy-chain` sont les premières versions de ce PCB qui devaient être produites avec la Voltera V-One, sans grand succès.

## Fonctionnalités
- Support physique pour le CAN
- Terminaisons amovibles
- Réutilisable
- Portable

## Améliorations possibles
- Reculer les connecteurs vers l'intérieur de la carte pour améliorer leur résistance aux chocs et limiter les déconnexions intempestives.
- Remplacer le transmetteur actuel par le [MCP2542FDT-E/SN](https://www.digikey.fr/fr/products/detail/microchip-technology/MCP2542FDT-E-SN/5975416) (utilisé par le [SPI-CAN Controller](https://gitlab.com/polybot-grenoble/pcb/prototype-can-controller-pi)) pour réduire le nombre de références à acheter.
- Remplacer les résistances 1206 WIDE par des résistances au format plus standard.
- Remplacer les capacités céramique 0805 par des 1206, plus simple à manipuler.
- Réutiliser le plus possible les composants entre les différents PCB.

## Nomenclature (BOM)
|Marqueur|Composant|Quantité|
|--------|---------|--------|
|J5, J6|[JST S2B-PH-K](https://www.digikey.fr/fr/products/detail/jst-sales-america-inc/S2B-PH-K-S/926626)|2|
|R1, R2|[Resistance 1206 WIDE 1.5W 60Ohm](https://www.digikey.fr/fr/products/detail/rohm-semiconductor/LTR18EZPF60R4/4052953)|2|
|C1|[Capacité céramique 1206 50V 4700pF](https://www.digikey.fr/fr/products/detail/kemet/C1206C472J5RAC7800/2215931)|1|
|C2|[Capacité céramique 0805 6.3V 0.1uF](https://www.digikey.fr/fr/products/detail/kyocera-avx/08056D104KAT2A/11568656)|1|
|J3, J4|Pins femelle 2x1 2.54mm|2|
|U1|[MCP2544WFDT-E/SN](https://www.digikey.fr/fr/products/detail/microchip-technology/MCP2544WFDT-E-SN/5975422)|1|
|A1|[Arduino Nano stackable headers](https://www.digikey.fr/fr/products/detail/sparkfun-electronics/PRT-16279/12178440)|2|

Ce document n'est pas définitif et les composants listés ci-dessus peuvent être modifiés sans préavis.

## Ressources
- [Documentation technique Nucleo L432KC](https://www.st.com/resource/en/user_manual/um1956-stm32-nucleo32-boards-mb1180-stmicroelectronics.pdf)
- [Documentation technique STM32L432KC](https://www.st.com/resource/en/datasheet/DM00257205.pdf)
- [Documentation technique MCP2542FD](https://ww1.microchip.com/downloads/en/DeviceDoc/MCP2542FD-MCP2542WFD-4WFD-Data-Sheet-DS20005514C.pdf)
- [Accès CADLAB](https://cadlab.io/project/27549)
- [Documentation principale](https://gitlab.com/polybot-grenoble/core/coredoc)